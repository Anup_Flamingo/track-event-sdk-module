import { connect } from "mongoose";

//Connecting to Database and Creating a Server
const connectToDatabase = async ()=>{
    try{
        return await connect(process.env.MONOGODB_URL);
    }
    catch(err){
        // console.log(err);
        return Promise.reject("Error in Conencting Database ",err)
    }
}

export default connectToDatabase
    
