import jwt from "jsonwebtoken"

const VerifyToken = (token)=>{
    try{
        const {userId} = jwt.verify(token, process.env.JWT_TOKEN_KEY)
        return userId
    }
    catch(err){
        return new Error("Invalid Token")
    }
}

export default VerifyToken