import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from "graphql";

const TrackEvent_Schema = new GraphQLObjectType({
    "name": "TrackEvent",
    fields: ()=>({
        message: {type: GraphQLString   }
    })
})

export default TrackEvent_Schema