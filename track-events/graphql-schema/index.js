import { GraphQLList, GraphQLNonNull, GraphQLObjectType, GraphQLSchema, GraphQLString } from "graphql";
import  CreateEvent_Schema  from "./CreateEvent_Schema.js";
import CreateEvent_Resolver from "../Resolvers/CreateEvent_Resolvers.js";
import GetAllEvents_Resolver from "../Resolvers/GetAllEvents_Resolver.js";
import TrackEvent_Schema from "./TrackEvent_Schema.js";
import TrackEvent_Resolver from "../Resolvers/TrackEvent_Resolver.js";
import GetTrackEvent_Schema from "./GetTrackEvent_Schema.js";
import GetTrackEvents_Resolver from "../Resolvers/GetTrackEvent_Resolvers.js";
import Users_Schema from "./Users_Schema.js";
import Signup_Resolver from "../Resolvers/Signup_Resolver.js";
import Token_Schema from "./Token_Schema.js";
import LoginUser_Resolver from "../Resolvers/LoginUser_Resolver.js";
import Aggregate_Schema from "./Aggregate_Schema.js";
import Aggregate_Resolver from "../Resolvers/Aggregate_Resolver.js";

const RootQuery = new GraphQLObjectType({
    name: "RootQuery",
    fields: ()=>({
        getAllEvents : {
            type: new GraphQLList(CreateEvent_Schema),
            resolve: GetAllEvents_Resolver
        },
        getEventInfo:{
            type: GetTrackEvent_Schema,
            args:{
                parametersObjectJSON : {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve: GetTrackEvents_Resolver
        },
        getAggregatedData:{
            type: new GraphQLList(Aggregate_Schema),
            args:{
                minDate : {type: GraphQLString},
                maxDate : {type: GraphQLString}
            },
            resolve: Aggregate_Resolver
        }
    })
})


const Mutations = new GraphQLObjectType({
    name: "Mutation",
    fields:()=>({
        createEvent:{
            type:  CreateEvent_Schema,
            args:{
                event_name: {type : new GraphQLNonNull(GraphQLString)}
            },
            resolve: CreateEvent_Resolver
        }, 
        trackEvent:{
            type: TrackEvent_Schema,
            args:{
                event_name: {type : new GraphQLNonNull(GraphQLString)},
                parameterObjJSON: {type : new GraphQLNonNull(GraphQLString)},
            },
            resolve: TrackEvent_Resolver
        },
        signupUser:{
            type: Users_Schema,
            args:{
                email: {type : new GraphQLNonNull(GraphQLString)},
                password:{type: new GraphQLNonNull(GraphQLString)}
            },
            resolve: Signup_Resolver
        },
        loginUser:{
            type: Token_Schema,
            args:{
                email: {type : new GraphQLNonNull(GraphQLString)},
                password:{type: new GraphQLNonNull(GraphQLString)}
            },
            resolve: LoginUser_Resolver
        }
    })
})

export const GraphQL_Schema  = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutations
})
