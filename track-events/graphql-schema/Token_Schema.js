import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from "graphql";

const Token_Schema = new GraphQLObjectType({
    name: "Token",
    fields: ()=>({
        token : {type: new GraphQLNonNull(GraphQLString)}
    })
})

export default Token_Schema