import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from "graphql";

const GetTrackEvent_Schema = new GraphQLObjectType({
    name: "GetTrackEventData",
    fields: ()=>({
        paramObjectJSON : {type: GraphQLString},
        error: {type: GraphQLString}
    })
})

export default GetTrackEvent_Schema;