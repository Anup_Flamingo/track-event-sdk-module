import {GraphQLID, GraphQLNonNull, GraphQLObjectType, GraphQLString} from 'graphql'

const CreateEvent_Schema = new GraphQLObjectType({
    name:"CreateEvent",
    fields:()=>({
        id: {type:  GraphQLID},
        userId: {type: new GraphQLNonNull(GraphQLString)},
        event_name: {type: new GraphQLNonNull(GraphQLString)},
        updatedAt: {type: GraphQLString}
    })
})

export default CreateEvent_Schema