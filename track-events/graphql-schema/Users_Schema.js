import { GraphQLNonNull, GraphQLObjectType, GraphQLString } from "graphql";

const Users_Schema = new GraphQLObjectType({
    name: "Users",
    fields:()=>({
        _id: {type: new GraphQLNonNull(GraphQLString)},
        email: {type : new GraphQLNonNull(GraphQLString)},
        password : {type : new GraphQLNonNull(GraphQLString)}
    })
})

export default Users_Schema