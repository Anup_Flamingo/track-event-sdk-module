import { GraphQLInt, GraphQLNonNull, GraphQLObjectType, GraphQLScalarType, GraphQLString } from "graphql";

const Aggregate_Schema = new GraphQLObjectType({
    name: "Aggregate",
    fields: ()=>({
        _id: {type : new GraphQLNonNull(GraphQLString)},
        event_name: {type :  new GraphQLNonNull(GraphQLString)},
        event_id: {type:  new GraphQLNonNull(GraphQLString)}, 
        totalCount: {type:  GraphQLInt},
        uniqueCount: { type: GraphQLInt},
    })
})

export default  Aggregate_Schema;