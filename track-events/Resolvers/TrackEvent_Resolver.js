import TrackEvent_Model from "../Models/TrackEvent_Model.js";
import Error_Model from "../Models/Error_Model.js";
import VerifyToken from "../utils/VerifyToken.js";
import CreateEvent_Model from "../Models/CreateEvent_Model.js";

const TrackEvent_Resolver = async (parent, args, {secretValue})=>{
    try{
        const userId = VerifyToken(secretValue.authorization)

        if(userId !== "Invalid Token"){
            const {event_name, parameterObjJSON} = args;
            
            const eventInfo = await CreateEvent_Model.findOne({userId, event_name})

            if(!eventInfo){
                return new Error("Event does not exist")
            }

            // const trackEvent = await TrackEvent_Model.findOne({event_id: eventInfo?.id})

            // if(trackEvent){
            //         if(trackEvent.customerId.includes(customerId)){
            //             await trackEvent.updateOne( {$inc: { totalCount : 1 } })
            //         }
            //         else{
            //             const newUpdate = {
            //                 customerId: customerId?[...trackEvent.customerId, customerId]: [...trackEvent.customerId],
            //                 totalCount: trackEvent.totalCount + 1,
            //                 uniqueCount: trackEvent.uniqueCount + 1
            //             }
            //             await trackEvent.updateOne({$set: newUpdate}) 
            //         }   

            // }
            // else{
            //     const paramterObject = JSON.parse(parameterObjJSON);
                
            //     const payload = {
            //         event_name,
            //         customerId: customerId?[customerId]: [],
            //         event_id : eventInfo?.id,
            //         hostName
            //     }
            //     for(let key in paramterObject){
            //         payload[key] = paramterObject[key]
            //     }
        
            //     console.log(payload);
        
            //     await new TrackEvent_Model(payload).save()
            // }

            const paramterObject = JSON.parse(parameterObjJSON);
                
                const payload = {
                    event_name,
                    event_id : eventInfo?.id,
                }
                for(let key in paramterObject){
                    payload[key] = paramterObject[key]
                }
        
                console.log(payload);
        
                await new TrackEvent_Model(payload).save()

            
            return {"message": "Success"}

        }
        else{
            return new Error(userId)
        }


    }
    catch(err){
         Error_Model({
            "error": err,
            "model" : "TrackEvent_Model"
        }).save()
        return {
            error: "Error in TrackEvent_Resolver"
        }
    }

}

export default TrackEvent_Resolver;