import CreateEvent_Model from "../Models/CreateEvent_Model.js";
import Error_Model from "../Models/Error_Model.js";
import VerifyToken from "../utils/VerifyToken.js";

const GetAllEvents_Resolver =  (parent, args, {secretValue})=>{
    try{

        const userId = VerifyToken(secretValue.authorization);
        if(userId?.message !== "Invalid Token"){
            return  CreateEvent_Model.find({userId})
        }else{
            return userId;
        }
    }
    catch(err){
        Error_Model({
            error: err,
            model: "GetAllEvents_Resolver"

       }).save()
       return new Error("Error in GetAllEvents")
    }
}

export default GetAllEvents_Resolver;