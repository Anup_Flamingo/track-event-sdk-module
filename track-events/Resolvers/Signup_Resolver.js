import Error_Model from "../Models/Error_Model.js";
import Users_Model from "../Models/Users_Model.js";
import bcrypt from "bcrypt"

const Singup_Resolver = async (parent, args)=>{
    try{
        const {email, password} = args;

        const hasEmail = await Users_Model.findOne({email});
            console.log(hasEmail);
        if(hasEmail){
            return new Error("Email ID already exist")
        }

        const hashedPassword = await bcrypt.hash(password, 10);

        return new Users_Model({
            email,
            password: hashedPassword
        }).save()
    }
    catch(err){
        Error_Model({
            "error": err,
            "model" : "Users_Model"
        }).save();

        throw Error("Error While Singing up")
    }
}

export default Singup_Resolver