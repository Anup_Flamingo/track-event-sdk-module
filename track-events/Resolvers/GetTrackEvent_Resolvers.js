import CreateEvent_Model from "../Models/CreateEvent_Model.js";
import Error_Model from "../Models/Error_Model.js";
import TrackEvent_Model from "../Models/TrackEvent_Model.js";
import VerifyToken from "../utils/VerifyToken.js";

const GetTrackEvents_Resolver = async (parent, args, {secretValue})=>{

    try{
        const userId = VerifyToken(secretValue.authorization)
        if(userId?.message !== "Invalid Token"){
            if(args.parametersObjectJSON.length >0){
                const parsedObjectJSON = JSON.parse(args.parametersObjectJSON);
                const data = await TrackEvent_Model.find({...parsedObjectJSON, userId})
                return { paramObjectJSON : JSON.stringify(data)}
            }
            else{
                const allEventList = await CreateEvent_Model.find({userId})
                console.log(allEventList.length);
                const data = await Promise.all(allEventList.map(item=> TrackEvent_Model.find({event_id: item.id})))
                console.log(data);
                // const data = await TrackEvent_Model.find({event_id : allEventList})
                return { paramObjectJSON : JSON.stringify(data)}
            }
        }
        else{
            return new Error("Invalid Token")
        }

    }
    catch(err){
        Error_Model({
            "error":err,
            "model": "GetTrackEvent_Resovers"
        }).save();

        return{
            error: "Error in Getting Track Event"
        }


    }



}

export default GetTrackEvents_Resolver