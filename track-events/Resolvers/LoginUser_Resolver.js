import Error_Model from "../Models/Error_Model.js";
import Users_Model from "../Models/Users_Model.js";
import brcypt from "bcrypt";
import jwt from 'jsonwebtoken'

const LoginUser_Resolver = async(parent, args)=>{
    try{
        const JWT_TOKEN = process.env.JWT_TOKEN_KEY
        const {email, password} = args;
        const user = await Users_Model.findOne({email})

        if(!user){
            return new Error("Email Id does not exist")
        }

        const isPassword = await brcypt.compare(password, user.password);

        if(isPassword){
            const jwtToken = jwt.sign({userId: user._id}, JWT_TOKEN)

            return {token : jwtToken};
        }else{
            return new Error("Password is Invalid")
        }

    }
    catch(err){
        Error_Model({
            "error": err,
            "model" : "Users_Model"
        }).save();

        return new Error("Error while logging in")
    }
}

export default LoginUser_Resolver;