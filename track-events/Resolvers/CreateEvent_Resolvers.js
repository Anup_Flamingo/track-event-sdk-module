import CreateEvent_Model from "../Models/CreateEvent_Model.js";
import Error_Model from "../Models/Error_Model.js";
import VerifyToken from "../utils/VerifyToken.js";

const CreateEvent_Resolver = (parent, args, {secretValue})=>{
     try{

          const userId = VerifyToken(secretValue.authorization);
          if(userId?.message !== "Invalid Token"){
               args.userId = userId;
               return CreateEvent_Model({...args}).save()
          }else{
               return userId
          }
     }
     catch(err){
          Error_Model({
               error: err,
               model: "CreateEvent_Resolver"

          }).save()
          return new Error("Error in Creating Event")
     }
}

export default CreateEvent_Resolver
