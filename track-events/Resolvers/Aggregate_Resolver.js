import CreateEvent_Model from "../Models/CreateEvent_Model.js";
import Error_Model from "../Models/Error_Model.js";
import TrackEvent_Model from "../Models/TrackEvent_Model.js";
import VerifyToken from "../utils/VerifyToken.js";

const Aggregate_Resolver = async (parent, args,{secretValue})=>{

    try{
        const {minDate, maxDate} = args;
        const userId = VerifyToken(secretValue.authorization);

        if(userId?.message !== "Invalid Token"){
            const allEventList = await CreateEvent_Model.find({userId});
    
            let aggregatedData
            if(minDate && maxDate){
                aggregatedData = await Promise.all(allEventList.map(event=> TrackEvent_Model.aggregate([
                    // {$match:{event_name: event.event_name, updatedAt : { $gte: new Date(minDate).toISOString(), $lte: new Date(maxDate).toISOString() }}}, 
                    {$match:{event_name: event.event_name, createdAt : { $gte: new Date(minDate), $lt: new Date(maxDate) }}}, 
                    {$group: { _id: "$event_name", totalCount: { $sum: 1 } } },
                    { $sort: { totalCount: -1 } }])
                ))
            }
            else{
                aggregatedData = await Promise.all(allEventList.map(event=> TrackEvent_Model.aggregate([
                    {$match:{event_name: event.event_name}}, 
                    {$group: { _id: "$event_id", totalCount: { $sum: 1 } } },
                    { $sort: { totalCount: -1 } }])
                ))
            }

            console.log(aggregatedData);
            
            const refinedAggregateData = aggregatedData.reduce((acc, data)=>{
                
                if(data.length >0){
                    acc.push(data[0])
                }
    
                return acc
            },[])
    
            return refinedAggregateData
            
        }
        else{
            return new Error("Invalid Token")
        }
    }
    catch(err){
        Error_Model({
            "error":err,
            "model": "Agreegate_Resolvers"
        }).save();

        return new Error("Error in Aggregating data")

    }
    
}

export default Aggregate_Resolver