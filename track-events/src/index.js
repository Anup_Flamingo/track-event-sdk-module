import express from "express";
import { createHandler } from 'graphql-http/lib/use/express';
import {graphqlHTTP} from "express-graphql"
import graphqlHeader from "express-graphql-header"
import { config } from "dotenv";
import connectToDatabase from "../config/db.js";
import { GraphQL_Schema } from "../graphql-schema/index.js";
import open from "open"
config();

const app = express();
app.all("/graphql", graphqlHeader,  graphqlHTTP(req=>{

    const authorization = req.headers
    return {
        graphiql : process.env.STATUS === "development",
        schema: GraphQL_Schema,
        context:{
            secretValue: authorization
        }
    }
}))

const PORT = process.env.STATUS === "production"? process.env.PRODUCTION_PORT: process.env.DEVELOPMENT_PORT


// Connecting to Database
connectToDatabase()
.then((success)=>{
    console.log("Connected to Database");
})
.then(()=>{
    app.listen(PORT, ()=>{
        console.log("Server is Running on Port", PORT);
    })
    return open('http://localhost:'+ PORT+ '/graphql', {app: "google chrome"})
})
.catch(err=>{
    console.log(err);
})

