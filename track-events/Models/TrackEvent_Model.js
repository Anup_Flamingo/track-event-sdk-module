import {Schema, model} from 'mongoose'


const TrackEvent = new Schema({
    "event_name":{
        type: String
    },
    "event_id": {
        type: String,
        require: true
    }
},{
    timestamps: true,
    strict: false
})

export default model("track-event", TrackEvent)
