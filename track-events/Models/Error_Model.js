import { Schema, model } from "mongoose";

const Error_Model = new Schema({
    error: {type: String},
    model: {type: String}
}, {
    timeseries: true
})



export default model("error", Error_Model)