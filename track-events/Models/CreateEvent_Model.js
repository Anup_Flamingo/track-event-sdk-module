import { Schema as _Schema, model } from 'mongoose';

const Schema = _Schema;

const CreateEvent = new Schema(
    {
        event_name:{
            type: String,
            required: true,
            unique: true

        },
        userId:{
            type: String,
            required: true
        }
    },
    {timestamps: true}
)

export default model("create-event", CreateEvent)