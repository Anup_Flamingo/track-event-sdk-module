import { Schema, Schema as _Schema, model } from "mongoose";

const Users = new Schema({
    email:{
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})

export default model("Users", Users)