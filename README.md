# Start the Server 
- npm install
- npm run start

## Modules Required
- **dotenv**
- **nodemon** 
- **express**
- **mongoose**
- **body-parser**
- **graphql**
- **graphql-http**
- **express-graphql**

## Folders
1. **Models** - MonogoDB Model is Defined
2. **graphql-schema** - GraphQL Schema has been Defined
3. **Resolvers** - Logic for handling request and sending response is written
